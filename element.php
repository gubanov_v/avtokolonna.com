<?
function create_reviews($page, $company)
{
    $str = '';

    if ($page && $company && isset($company["ID"]))
    {
        $config = config::getInstance();
        
        $review_count = $config->getMas("review_count");
        $default_img = $config->getPicture("review_preview_picture");
        
        $dbReviews = new iblock($config->getBlocksId("reviews"));
        $dbReviews->setFilter(array("PROPERTY_COMPANY" => $company["ID"]));
        
        $all_reviews = count($dbReviews->getList());      
        
        if ($all_reviews)
        {
            $dbReviews->setPage($page, $review_count);
            $dbReviews->setOrder(array("ACTIVE_FROM" => "DESC"));
            $reviews = $dbReviews->getList();        
            
            foreach ($reviews as $review) 
            { 
                $img = tools::cut_picture($review["PREVIEW_PICTURE"], $review["NAME"], 
                    $default_img["dimensions"], 
                        $default_img["src"]);
                                
                if ($review["RATE"] == "Отлично")
                {
                    $add = "good";
                    $rating_value = 5;
                }
                else
                {
                    $add = "bad";
                    $rating_value = 1;
                }

                $date = $review["ACTIVE_FROM"];                    
                $date = explode(" ",$date);
                $date_str = explode(".", $date[0]);
                
                $file = CFile::GetFileArray($company["PREVIEW_PICTURE"]);
                    
                $str .= '<div class="comment-card" itemscope itemtype="http://schema.org/Review">
                    <a itemprop="url" href="http://'.$_SERVER["HTTP_HOST"].$_SERVER["REQUEST_URI"].'"></a>'.
                    $img.
                    '<div class="info">
                      <span class="icon-comment icon-comment--'.$add.'" itemprop="reviewRating" itemscope itemtype="http://schema.org/Rating">
                            <meta itemprop="worstRating" content="1">
                            <meta itemprop="bestRating" content="5">
                            <meta itemprop="ratingValue" content="'.$rating_value.'">'.$review["RATE"].'</span>
                      <span class="client-name" itemprop="author" itemscope itemtype="http://schema.org/Person"><span itemprop="name">'.$review["NAME"].'</span> ('.$date[0].')</span>
                      <meta itemprop="datePublished" content="'.$date_str[2].'-'.$date_str[1].'-'.$date_str[0].'">
                      <p itemprop="reviewBody">'.strip_tags($review["PREVIEW_TEXT"]).'</p>
                      
                      <div itemprop="itemReviewed" itemscope itemtype="http://schema.org/Organization">
                        <span itemprop="address" itemscope itemtype="http://schema.org/PostalAddress"><meta itemprop="streetAddress" content="'.$company["ADDRESS"].'"></span>
                        <meta itemprop="name" content="'.htmlspecialchars($company["NAME"]).'">
                        <a itemprop="officialWebPage" href="'.$company["LINK"].'"></a>
                        <a itemprop="url" href="'.$company["LINK"].'"></a>
                        <meta itemprop="telephone" content="'.$company["PHONE"].'">
                        <img itemprop="image" src="http://'.$_SERVER["HTTP_HOST"].$file["SRC"].'">
                      </div>
                      
                    </div>
                      <div class="line"></div>
                   </div>';
            }
            
            if ($page * $review_count < $all_reviews)
            {
                $str .= '<input name="page" type="hidden" value="'.$page.'">
                    <a href="#" class="btn btn-wide btn-empty btn-more feedback-more">Еще</a>';
            }
        }
        else
        {
            $str .= '<p>Ваш отзыв будет первым.</p>
            <div class="no-feedback">
           <img src="'.SITE_TEMPLATE_PATH.'/images/zaglushka.png" alt="">
        </div>';
        }
    }
    return $str;   
}

if ($_POST && $_POST["op"])
{
    switch ($_POST["op"])
    {
        case "get_reviews":
            echo json_encode(array("code" => "success", 
                "answer" => create_reviews($_POST["args"]["page"], $_POST["args"]["company"]), "str" => ""));
        break;
        case "record_review":
                
             $config = config::getInstance();
             $iblock = $config->getBlocksId("reviews");
             
             foreach ($_POST["args"] as $key => $value)
                $_POST["args"][$key] = trim($value);
                   
             $el = new CIBlockElement;
             
             $arEl = array("IBLOCK_ID" => $iblock,
                                "ACTIVE" => "N", 
                                "DATE_ACTIVE_FROM" => ConvertTimeStamp(time(), "SHORT"),
                                "NAME" => $_POST["args"]["name"],
                                "PREVIEW_TEXT" => $_POST["args"]["text"],
                                "PROPERTY_VALUES" => array(
                                    "INFO" =>  $_POST["args"]["info"], 
                                    "COMPANY" => $_POST["args"]["company"], 
                                    "RATE" => $_POST["args"]["rate"],
                                    ));
                        
            if ($file = $_FILES["file"])
            {
                $arEl["PREVIEW_PICTURE"] = $file;
            } 
             
            if ($el->Add($arEl) === false)
            {
                $code = "error";
                $str = strip_tags($el->LAST_ERROR);
            }
            else
            {
                $code = "success";
                $str = "Ваш отзыв добавлен и будет опубликован после проверки модератором.";
            }
            
            echo json_encode(array("code" => $code, 
                "answer" => "", "str" => $str));
        break;
    }
    exit();
}

$config = config::getInstance();

$this->addCssFile("carousel.css");

$dbCompainies = new iblock($config->getBlocksId("companies"));
$dbCompainies->setFilter(array("ID" => $config->getId()));
$dbCompainies->setOrder(array("PROPERTY_RATE" => "DESC"));
$company = current($dbCompainies->getList());

$dbReviews = new iblock($config->getBlocksId("reviews"));
$dbReviews->setFilter(array("PROPERTY_COMPANY" => $config->getId()));

$all_reviews = count($dbReviews->getList());   

$dbReviews->setOrder(array("DATE_CREATE" => "DESC"));
$dbReviews->setCount(1);
$review = current($dbReviews->getList());

$dbBlocks = new iblock($config->getBlocksId("blocks"));
$dbBlocks->setFilter(array("CODE" => array("reviews_info")));
$dbBlocks->setUniq("CODE");
$blocks = $dbBlocks->getList();


if (!$this->_flag_title)
    $this->_title = $company["NAME"]." - отзывы о службе эвакуации ".$company["NAME"];

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_after.php");

$this->addJsFile("jquery.jcarousel.min.js");
$this->addJsFile("element.js");

?>

<section class="hello">
      <div class="fixblock">
          
        <div class="crumbs">
            <a href="/"><img src="<?=SITE_TEMPLATE_PATH?>/images/icon-home.png" alt="Главная" title="Главная" width="17" height="15"/></a>
            <span><?=$company["NAME"]?></span>
           
        </div>
        
        <div class="company-card">

          <div class="visual">
            <div class="logo-card">
               <? $default_img = $config->getPicture("company_preview_picture");
        
                 $img = tools::cut_picture($company["PREVIEW_PICTURE"], $company["NAME"], 
                    $default_img["dimensions"], 
                        $default_img["src"]);
                ?>
                <?=$img?>
            </div>
            <? $add_sup = "";
               if ($company["RATE_CORRECT"] > 0)
               {
                    $add_sup = "+".(string) $company["RATE_CORRECT"];
                    $add_sup_class = "up";
               }
               
               if ($company["RATE_CORRECT"] < 0)
               {
                    $add_sup = (string) $company["RATE_CORRECT"];
                    $add_sup_class = "down";
               }
                    
               $add_sup = '<sup class="'.$add_sup_class.'">'.$add_sup.'</sup>';
            ?>
            <p class="visual-rating">Рейтинг <span><?=$company["RATE"]?><?=$add_sup?></span></p>
          </div>

          <div class="content">
            <div class="about">

              <div class="info">
                <h1><?=$company["NAME"]?></h1>
                

                <ul class="services-options">
                <? if ($company["AVTO"]):?>
                    <li>Автопарк ~ <span><?=tools::declOfNum($company["AVTO"], array("эвакуатор", "эвакуатора", "эвакуаторов"));?></span></li>
                  <? endif;?>
                  <? if ($company["TIME"]):?>
                    <li>Время подачи ~ <span><?=$company["TIME"]?></span></li>
                  <? endif;?>
                          <li>Отзывы - <a href="#" class="a-all">(<?=tools::declOfNum($all_reviews, array("отзыв","отзыва","отзывов"))?>)</a></li>
                </ul>
              </div>

              <div class="contacts-block">
                <a class="h1" href="tel:<?=tools::cut_phone($company['PHONE'])?>"><?=$company["PHONE"]?></a>
            
                 <? if($company["ADDRESS"]):?>
                    <p>Адрес: <?=$company["ADDRESS"]?></p>
                 <? endif; ?>
                 <? if($company["SHEDULE"]):?>
                    <p>График работы: <?=$company["SHEDULE"]?></p>
                  <? endif; ?>
                  <? if($company["ZONE"]):?>
                    <p>Зона обслуживания: <?=$company["ZONE"]?></p>
                 <? endif; ?>  
              </div>
            </div>

            <div class="services">
              <div class=" services-list list">
                 <? if ($company["SERVICE"]): ?>
                <p class="header">Стоимость услуг:</p>
                <div class="list services-list ">
                    <?=$company["SERVICE"]["TEXT"]?>
                </div>
                <? endif;?>
              </div>
              <? if (!empty($review))
                {
                    if ($review["RATE"] == "Отлично")
                        $add = "good";
                    else
                        $add = "bad";
                }
              ?>      
              <div class="comments-block">
              <p class="header">О компании:</p>
               <p class="services-about"><?=strip_tags($company["PREVIEW_TEXT"])?></p>
                </div>
             </div>
           </div>
        </div>
          
        <div class="carousel-wrapper">
            <div class="carousel-prev"></div>
            <div class="carousel-content">
                <? 
                $slider = array();
                $dbCompainies = new iblock($config->getBlocksId("companies"));
                $dbCompainies->setOrder(array("PROPERTY_RATE" => "DESC"));
                //$dbCompainies->setCount(15);
                $companies = $dbCompainies->getList();
                $i = 0;
                $current = false;
                   
                foreach ($companies as $value)
                {
                    if ($company["CODE"] == $value["CODE"]) $current = $i;
                    $slider[] = $value;
                    $i++;
                }
                        
                ?>
                <ul>
                    <? $j = 0; 
                       $count_companies = count($companies);
                       if ($current!== false && $current > 2) 
                            $j = $current - 2;
                            
                       if ($current!==false && $current > ($count_companies - 3))
                            $j = $count_companies - 5;
                                 
                    for ($i=0;$i<$count_companies;$i++)
                    {
                        if ($j > ($count_companies-1)) $j = 0; 
                       
                       $value = $slider[$j];
                        
                       $active = ($current === $j) ? " active" :  ""; ?>
                    <li>
                        <a href="<?=('/' . $value["CODE"] . '/')?>" class="slider-marker">
                          <div class="img-container<?=$active?>">
                            <?  $default_img = $config->getPicture("company_slide_picture");
                                $img = tools::cut_picture($value["PREVIEW_PICTURE"], $value["NAME"], 
                                $default_img["dimensions"], 
                                    $default_img["src"]);?>
                            <?=$img?>
                          </div>
                          <p>Рейтинг <span><?=$value["RATE"]?></span></p>
                        </a>
                    </li>
                    <? $j++;
                    }?>
                </ul>
            </div>
            <div class="carousel-next"></div>
        </div>          
       </div>
    </section>

    <div class="fixblock all-feedback"> 
      <div class="block">
       <div class="title">
          <h2>Отзывы о <?=$company["NAME"]?></h2>
          <a href="#" class="a-feedback">Оставить отзыв</a>
        </div>
        
        
         
        <div class="comments-block comments-list">
           <?=create_reviews(1, $company);?>           
         </div>
     </div>
   </div>
  
  <div class="give-feedback">
    <div class="fixblock">
      <div class="block">
        <p class="h2">
          Оставить отзыв о <?=$company["NAME"]?>
        </p>
        
        <form class="form">
          <div class="field-block">
              <div class="field-block--input">
              <p>Как вас зовут:</p>
              <input class="required" type="text" placeholder="Как вас зовут?" name="args[name]"/>
            </div>
             
            <div class="field-block--input">
              <p>E-mail или телефон:
                
              </p>
              <input class="required" type="text" placeholder="Ваша почта или телефон" name="args[info]"/>
            </div>
            
            <div class="field-block--textarea">
              <p>Ваш отзыв:</p>
              <textarea class="required" rows="5" placeholder="Введите ваш отзыв" name="args[text]"></textarea>
            </div>
        
            <div>
              <span class="required-before required-field">обязательные поля</span>
              <div class="item-article ">
                <img src="<?=SITE_TEMPLATE_PATH?>/images/icon-info.png" class="" alt="Инфо" width="15" height="15"/>
                <? if ($blocks["reviews_info"]["DETAIL_TEXT"]) :?>
                <div class="info">
                  <?=$blocks["reviews_info"]["DETAIL_TEXT"]?>
                </div>
                <? endif; ?>
              </div>
            </div>
          </div>
          
          <div class="other-block">
             <div class="download-block">
              <p>Загрузить фотографию:
              <a href="#" class="a-info"><img src="<?=SITE_TEMPLATE_PATH?>/images/icon-info.png" class="" alt="Инфо" width="15" height="15"/></a></p>
              
              <div class="download-input">
                <span class="btn btn-wide btn-empty">Прикрепить файл<input type="file" accept=".jpg,.jpeg,.gif,.png" name="file">
                 </span>
                 <div class="file-list">
                   <a href="#" style="display: none;"></a>        
                 </div>
              </div>
               
              <p class="text-lite">Вы можете загрузить изображение в формате JPG, GIF или PNG.</p>
            </div>
        
            <div class="rating-block">
              <p class="required-after">Ваша оценка компании:</p>
              <div class="rating-button">
                <input type="radio" value="1" name="args[rate]" id="rate1"><label for="rate1" class="btn btn-rating btn-rating--good label-rate">Отлично</label>
                <input type="radio" value="2" name="args[rate]" id="rate2"><label for="rate2" class="btn btn-rating btn-rating--bad label-rate">Плохо</label>
              </div>
            </div>
            
            <p class="error-message error"></p>
            
            <a href="#" class="btn btn-wide btn-green submit">Отправить</a>
          </div>
          
          <input name="args[company]" type="hidden" value="<?=$config->getId()?>"/>
          <input name="op" type="hidden" value="record_review"/>
        </form>        
        <a href="#" class="btn btn-wide btn-up">Наверх</a>
       </div>
       <div class="thanks">
          <img src="<?=SITE_TEMPLATE_PATH?>/images/thanks.png" alt="Спасибо" width="428" height="432"/>
            <div class="line"></div>
              <div class="info">
                <p class="h1">Спасибо за ваш отзыв</p>
                <p>Отзыв появится на сайте после того как пройдет ручную модерацию</p>
              </div>
        </div>
    </div>
  </div>


<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/epilog_before.php");
?>
