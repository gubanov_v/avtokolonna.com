<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <? if ($this->_description):?>
            <meta content="<?=$this->_description?>" name="description">
        <? endif; ?>
        
        <? if ($this->_keywords):?>
            <meta content="<?=$this->_keywords?>" name="keywords">
        <? endif; ?>
        
		<title><?=$this->_title;?></title>
        <link href='https://fonts.googleapis.com/css?family=Roboto:400,100,300,500,700&subset=latin,cyrillic' rel='stylesheet' type='text/css'>
        <link href='https://fonts.googleapis.com/css?family=Roboto+Condensed:400,300,700&subset=latin,cyrillic' rel='stylesheet' type='text/css'>
        <? 
        $this->addJsFile("jquery-2.2.4.min.js");
        $this->addJsFile("jquery.events.touch.js");
        $this->addJsFile("lightcase.js");
        $this->addJsFile("main.js");

        //$this->addCssFile("reset.css");
        
        $this->addCssFile("style.css");
        $this->addCssFile("media.css");

        ?>
        <link rel="icon" type="image/ico" href="<?=SITE_TEMPLATE_PATH?>/images/favicon.ico"/>
        <? if ($css = $this->getCss()): ?> 
            <link href="/min/f=<?=$css;?>&123456" rel="stylesheet" type="text/css"/>
        <? endif; ?>
	</head>
	<body>

<div class="fixblock page-404">
    
            <a class="logo" href="index.html"></a>
            <p class="h1">Ошибка 404</p>
            <p>Неправильно набран адрес или такой страницы больше не существует.</p>
            
            
                   <a href="/">На главную</a>
      
                       
<!--
        <form class="search-form">
       
            <div class="search-container">
                <div class="search-input">
                    <input class="search" placeholder="Компания" type="text">
                     <ul>
                        <li><a href="">text</a></li>
                        <li><a href="">text</a></li>
                        <li><a href="">text</a></li>
                        <li><a href="">text</a></li>
                        <li><a href="">text</a></li>
                        <li><a href="">text</a></li>
                        <li><a href="">text</a></li>
                    </ul>
                </div>
                <a id="search" class="btn btn-fill btn-m  btn-search" type="submit">найти</a>
            </div>
             
        </form>
-->

        
        
    </div>
    </body>
</html>