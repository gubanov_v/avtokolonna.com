<?
if ( !empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest' )
{
    if ( $_POST && $_POST["op"] )
    {
        switch ( $_POST["op"] )
        {
            case "send-partner":
                
                require_once ($_SERVER["DOCUMENT_ROOT"] . "/phpmailer/class.phpmailer.php");
                $config = config::getInstance();
                $data = array();
                
                foreach ($_POST["args"] as $key => $value)
                {
                    $data[$key] = trim($value);
                }

                $file = $_FILES["file"];

                $mailer = new PHPMailer();

                $htmlBody = "<html>
                            <head>
                                <title>Заявка на звонок для {$data["name"]}</title>
                                <link rel='stylesheet' href='http://avtoreshenie.infosted.bget.ru/bitrix/templates/avtoreshenie/css/mail.css'>
                            </head>
                            <body>
                                <a href='http://www.avtokolonna.com/' class='logo'>АВТОКОЛОННА</a>
                                <p>Название компании {$data["name"]}<br />
                                <p>Ссылка на сайт {$data["site"]}<br />
                                <p>Адрес {$data["address"]}<br />
                                <p>Зона обслуживания {$data["zone"]}<br />
                                <p>Номер телефона {$data["phone"]}<br />
                                <p>График работы {$data["graphic"]}<br />";

                $textBody = "Название компании {$data["name"]}\n
                                <p>Ссылка на сайт {$data["site"]}\n
                                <p>Адрес {$data["address"]}\n
                                <p>Зона обслуживания {$data["zone"]}\n
                                <p>Номер телефона {$data["phone"]}\n
                                <p>График работы {$data["graphic"]}\n";

                $text = array();
                $keys = array("autopark" => "Автопарк", "givetime" => "Время подачи", 
                              "car" => "Легковые автомобили", "autotruck" => "Грузовые автомобили", 
                              "motorcycle" => "Мотоциклы", "jeep" => "Джипы", "ringroad" => "Эвакуация за МКАД"
                             );
                
                foreach ($data as $key => $value)
                {
                    if ( !empty($data[$key]) && !empty($keys[$key]) ) 
                    {
                        $text[] = $keys[$key] . " " . $value;   
                    }
                }
                               
                $textBody .= implode('\n', $text) . '\n';
                $htmlBody .= implode('<br />', $text) . '</p></body></html>';

                $mailer->Priority = 3;
                $mailer->From = "info@avtokolonna.com";
                $mailer->Sender = "info@avtokolonna.com";
                $mailer->FromName = "АВТОКОЛОННА.COM";
                $mailer->Subject = "АВТОКОЛОННА.COM";
                $mailer->CharSet = "UTF-8";
                $mailer->Body = $htmlBody;
                $mailer->IsHTML(true);
                $mailer->AltBody = $textBody;
                $mailer->IsHTML(false);
                $mailer->AddAttachment($file["tmp_name"], $file["name"]);
                $mailer->AddAddress("narizhniydv@mail.ru", "Админ");

                $res = $mailer->Send();


                if ( !$res )
                {
                    echo json_encode(array("code" => "error", "answer" => "",
                                            "str" => "Приносим свои извинения - зарегистрировать Вас не удалось. Вы можете зарегистрироваться по телефону"),
                        JSON_FORCE_OBJECT | JSON_UNESCAPED_UNICODE );
                }
                else
                {
                    echo json_encode(array("code" => "success", "answer" => $file, "str" => "Ваша заявка успешно отправлена. Ожидайте звонка специалиста."),
                                     JSON_UNESCAPED_UNICODE | JSON_FORCE_OBJECT);
                }

                $mailer->ClearAddresses();
                $mailer->ClearAttachments();
                break;
        }
    }
    
    exit();
}

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_after.php");

$this->addJsFile("static.js");
?>

<section class="hello">
      <div class="fixblock">
         <div class="item-between">
               <div class="crumbs">
                <a href="/"><img src="<?=SITE_TEMPLATE_PATH?>/images/icon-home.png" alt="Главная" title="Главная" width="17" height="15"/></a>
                <span><?=$this->_h1?></span>
               
              </div>
              <? $menu = array("mission" => "Миссия", "partners" => "Партнеры",
                                "about" => "О проекте");
                 
                ?>
              <div class="menu menu-static">
               <? foreach ($menu as $key => $value)
                    {
    if ($key == $arg) {
        echo '<span class="active">'.$value.'</span>';
    }
    else {
        echo '<a href="/'.$key.'/">'.$value.'</a>';
    }
              
                    }?>
              </div>
          </div>

          <h1><?=$this->_h1?></h1> 
          <article>
            <?=$this->_raw_text;?>
            <div class="clear"></div>
          </article>      
    </div>
</section>


<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/epilog_before.php");
?>