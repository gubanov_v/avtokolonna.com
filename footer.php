    </main>
    
  <footer>
    <div class="fixblock">
      <div class="item-between">
          <div class="tech-info">
           <!-- noindex --><p class="text-lite">Любая информация на сайте www.avtokolonna.com не является публичной офертой ни при каких условиях и носит исключительно информационный характер. Информация о компаниях взята из открытых источников.</p>
<!-- Yandex.Metrika informer -->
<a href="https://metrika.yandex.ru/stat/?id=24376606&amp;from=informer"
target="_blank" rel="nofollow"><img src="https://informer.yandex.ru/informer/24376606/3_0_FFFFFFFF_F4F4F4FF_0_pageviews"
style="width:88px; height:31px; border:0;" alt="џндекс.Њетрика" title="џндекс.Њетрика: данные за сегоднЯ (просмотры, визиты и уникальные посетители)" onclick="try{Ya.Metrika.informer({i:this,id:24376606,lang:'ru'});return false}catch(e){}" /></a>
<!-- /Yandex.Metrika informer -->
            <!-- /noindex -->
          </div>
          <div class="menu menu-footer">
            <a href="/">Главная</a>
             <a href="/mission/">Миссия</a>
             <a href="/partners/">Партнеры</a>
             <a href="/about/">О проекте</a>
             <a href="/contact/">Контакты</a>
              <div class="rights">
                  <span>Все права защищены. © <?=date("Y");?></span>
                          <a href="/sitemap/" class="link link-sitemap"><img src="<?=SITE_TEMPLATE_PATH?>/images/icon-site-map.png" alt="Карта сайта" title="Карта сайта" width="16" height="13"></a>
              </div>
          </div>
      </div>
    </div>   
  </footer>
    
   <? if ($js = $this->getJs()): ?> 
        <script async type="text/javascript" src="/min/f=<?=$js;?>&123456"></script>
    <? endif; ?>

<!-- Yandex.Metrika counter -->
<script type="text/javascript">
    (function (d, w, c) {
        (w[c] = w[c] || []).push(function() {
            try {
                w.yaCounter24376606 = new Ya.Metrika({
                    id:24376606,
                    clickmap:true,
                    trackLinks:true,
                    accurateTrackBounce:true,
                    webvisor:true
                });
            } catch(e) { }
        });

        var n = d.getElementsByTagName("script")[0],
            s = d.createElement("script"),
            f = function () { n.parentNode.insertBefore(s, n); };
        s.type = "text/javascript";
        s.async = true;
        s.src = "https://mc.yandex.ru/metrika/watch.js";

        if (w.opera == "[object Opera]") {
            d.addEventListener("DOMContentLoaded", f, false);
        } else { f(); }
    })(document, window, "yandex_metrika_callbacks");
</script>
<noscript><div><img src="https://mc.yandex.ru/watch/24376606" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
<!-- /Yandex.Metrika counter -->

<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-80505641-1', 'auto');
  ga('send', 'pageview');

</script>
    
	</body>
</html> 