<!DOCTYPE html>
<html lang="ru">
	<head>
		<meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <? if ($this->_description):?>
            <meta content="<?=$this->_description?>" name="description">
        <? endif; ?>
        
        <? if ($this->_keywords):?>
            <meta content="<?=$this->_keywords?>" name="keywords">
        <? endif; ?>
        
		<title><?=$this->_title;?></title>
        <!--<link href='https://fonts.googleapis.com/css?family=Roboto:400,100,300,500,700&subset=latin,cyrillic' rel='stylesheet' type='text/css'>
        <link href='https://fonts.googleapis.com/css?family=Roboto+Condensed:400,300,700&subset=latin,cyrillic' rel='stylesheet' type='text/css'>-->
        <? 
        $this->addJsFile("jquery-2.2.4.min.js");
        $this->addJsFile("jquery.events.touch.js");
        $this->addJsFile("lightcase.js");
        $this->addJsFile("main.js");

        //$this->addCssFile("reset.css");
        
        $this->addCssFile("style.css");
        $this->addCssFile("media.css");

        if ($USER->GetID() == 1) 
            $ADMIN = true;
        else
            $ADMIN = false;
        
        if ($ADMIN)
        {
            $APPLICATION->ShowCSS();
            $APPLICATION->ShowHeadStrings();
            $APPLICATION->ShowHeadScripts();
        }
        ?>
        <link rel="icon" type="image/ico" href="<?=SITE_TEMPLATE_PATH?>/images/favicon.ico"/>
        <? if ($css = $this->getCss()): ?> 
            <link href="/min/f=<?=$css;?>&123456" rel="stylesheet" type="text/css"/>
        <? endif; ?>
	</head>
	<body>
    <?if ($ADMIN) $APPLICATION->ShowPanel(); ?> 
    <div class="top-block">
        <div class="fixblock">
            <div class="header">
                    <a href="/" class="logo"></a>
                    <div class="tagline">
                    <?
                        $dbBlocksGlobal = new iblock(config::getInstance()->getBlocksId("blocks"));
                        $dbBlocksGlobal->setFilter(array("CODE" => array("tagline", "phones")));
                        $dbBlocksGlobal->setUniq("CODE");
                        $blocksGlobal = $dbBlocksGlobal->getList();
                    ?>
                    <?=$blocksGlobal["tagline"]["DETAIL_TEXT"]?>
                    </div>
                    
<!--
                    <div class="contacts">
                      <div class="item-center">
                        <img src="<?=SITE_TEMPLATE_PATH?>/images/icon-phone.png" alt="Телефон" width="26" height="26">
                        <div class="info">
                          <?=$blocksGlobal["phones"]["DETAIL_TEXT"]?>
                        </div>
                      </div>
                    </div>
-->
                    
<!--
                    <div class="menu menu-header">
                          <a href="/mission/">Миссия проекта</a> | <a href="/partners/">Партнеры</a> | <a href="/about/">О проекте</a> | <a href="/contact/">Контакты</a>
                    </div>
-->
            </div>
        </div>
    </div>
    <main>
