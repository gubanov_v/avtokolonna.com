<?
$config = config::getInstance();

$this->addCssFile("carousel.css");
$params = $config->getParamsUrl();

$dbCounties = new iblock($config->getBlocksId("counties"));
$dbCounties->setFilter(array("CODE" => $params["county"]));
$county = $dbCounties->getList();

$dbCompainies = new iblock($config->getBlocksId("companies"));
$dbCompainies->setOrder(array("PROPERTY_RATE" => "DESC"));
$companies = $dbCompainies->getList();

$dbCountyCars = new iblock($config->getBlocksId("countycars"));
$dbCountyCars->setFilter(array("PROPERTY_COUNTY" => $county[0]["ID"]));
$countyCars = $dbCountyCars->getList();

$arrCountyCompanies = array();
$i = 0;

foreach ( $companies as $company )
{
    if ( $company["TOP"] == "да" )
    {
        foreach ( $countyCars as $countyCar )
        {
            if ( in_array($countyCar["ID"], $company["COUNTY_CARS"]) )
            {
                $arrCountyCompanies[$i] = $company;
                $arrCountyCompanies[$i]["COUNT_CARS"] = $countyCar["COUNT_CARS"];
                $i++;
                break;
            }
        }
    }
    
    if ( $i == 6 )
    {
        break;
    }
}

$blocks = array();
if ($county[0]["BLOCKS"])
{
    $dbBlocks = new iblock($config->getBlocksId("blocks"));
    $dbBlocks->setOrder(array("SORT" => "ASC"));
    $dbBlocks->setFilter(array("ID" => $county[0]["BLOCKS"]));
    $blocks = $dbBlocks->getList();
}

//echo '<div style="display:none">'.print_r($county["BLOCKS"], true).'</div>';

$dbBlocks = new iblock($config->getBlocksId("blocks"));
$dbBlocks->setFilter(array("CODE" => array("map")));
$dbBlocks->setUniq("CODE");
$blocks_dop = $dbBlocks->getList();

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_after.php");
echo '<script src="https://api-maps.yandex.ru/2.1/?lang=ru_RU" type="text/javascript"></script>';

$this->addJsFile("jquery.jcarousel.min.js");
$this->addJsFile("static-area.js");

?>

<section class="hello">
      <div class="fixblock">
         <div class="item-between">
               <div class="crumbs">
                <a href="/"><img src="<?=SITE_TEMPLATE_PATH?>/images/icon-home.png" alt="Главная" title="Главная" width="17" height="15"/></a>
                <span><?=$this->_h1?></span>
               
              </div>
            <? $menu = array("evacuatory-sao" => "САО", "evacuatory-svao" => "СВАО",
                                "evacuatory-vao" => "ВАО", "evacuatory-uvao" => "ЮВАО", "evacuatory-uao" => "ЮАО", "evacuatory-uzao" => "ЮЗАО" , "evacuatory-zao" => "ЗАО",  "evacuatory-szao" => "СЗАО", "evacuatory-cao" => "ЦАО");
                 
                ?>
              <div class="menu menu-static">
               <? foreach ($menu as $key => $value)
                    {
    if ($key == $arg) {
        echo '<span class="active">'.$value.'</span>';
    }
    else {
        echo '<a href="/'.$key.'/">'.$value.'</a>';
    }
              
                    }?>
              </div>

          </div>
          <? if ($blocks_dop["map"]):?>
             <div class="title">
                <h1><?=$blocks[0]["NAME"]?></h1>
                <div class="line"></div>
              </div>
          <? endif; ?>   
               <? 
                  $dbCars = new iblock($config->getBlocksId("cars"));
                  $dbCars->setUniq("ID");
                  $cars = $dbCars->getList();
             
                  $dbParkcars = new iblock($config->getBlocksId("parkcars"));
                  foreach ($dbParkcars->getList() as $park)
                  {
                      $count = count($park["POINT"]);
             
                      $time = floor(time() / (60 * 10));
             
                      $point = explode(",", $park["POINT"][$time % $count]);
             
                      echo '<div class="park" data-x="'.$point[0].'" data-y="'.$point[1].'" data-car="'.htmlspecialchars($park["CAR_NAME"]).'"
                          data-company="'.htmlspecialchars($park["COMPANY_NAME"]).'" data-company-code="'.$park["COMPANY_CODE"].'"></div>';
             
                  }
               ?>
               <div id="map" class="mymap" style="width: 100%; height: 580px"></div>
               
<!--               Районы -->
               <?=$blocks[0]["DETAIL_TEXT"]?>
               <div class="block"> <?=$blocks[3]["DETAIL_TEXT"]; ?></div>
                <!--         Конец Районы -->
                
<!--        TOP-5  -->
         <div class="block"> 
          <h2><?=$blocks[1]["NAME"]?></h2>
       <table>
       <thead>
          <tr>
            <th><img src="<?=SITE_TEMPLATE_PATH?>/images/icon-cup.png" alt="Кубок" width="21" height="22"/></th>
            <th>Название компании</th>
            <th>Автопарк</th>
            <th>Отзывы</th>
            <th>Цены</th>
            <th>Время подачи</th>
            <th>Рейтинг <a href="#" class="a-info"><img src="<?=SITE_TEMPLATE_PATH?>/images/icon-info.png" alt="Инфо" width="15" height="15"/></a></th>
            <th><p class="text-lite">Обновлено <? if ($blocks["update"]["DETAIL_TEXT"]) echo strip_tags($blocks["update"]["DETAIL_TEXT"]); else echo date("d.m.Y");?></p></th>
            </tr>
        </thead>
        <tbody>
        <?
            $i = 1;
            foreach ($arrCountyCompanies as $company) 
            {    
               if ($company["RATE_DYN"] == "Положительный")
                  $add = " icon-up";
               else
                if ($company["RATE_DYN"] == "Отрицательный")
                    $add = " icon-down";
                else
                    $add = "";
                    
               $add_sup = "";
               if ($company["RATE_CORRECT"] > 0)
               {
                    $add_sup = "+".(string) $company["RATE_CORRECT"];
                    $add_sup_class = "up";
               }
               
               if ($company["RATE_CORRECT"] < 0)
               {
                    $add_sup = (string) $company["RATE_CORRECT"];
                    $add_sup_class = "down";
               }
                    
               $add_sup = '<sup class="'.$add_sup_class.'">'.$add_sup.'</sup>';
               
               $company["LINK_NAME"] = ($company["LINK_NAME"]) ? $company["LINK_NAME"] : $company["NAME"];
        ?>
            <tr data-href="/<?=$company["CODE"]?>/">
                <td class="icon<?=$add?>"><?=$i?></td>
                <td><a href="/<?=$company["CODE"]?>/" class="link link-out"><?=$company["LINK_NAME"]?></a></td>
                <td>~<?=$company["COUNT_CARS"]?></td>
                <td><?=(($review_count[$company["ID"]]) ? $review_count[$company["ID"]] : 0)?></td>
                <td>от <?=$company["MIN_PRICE"]?> руб.</td>
                <td><?=$company["TIME"]?></td>
                <td><?=$company["RATE"]?><?=$add_sup?></td>
                <td><a href="/<?=$company["CODE"]?>/" class="btn btn-green btn-s btn-i-right">Подробнее</a></td>
            </tr>
        <? $i++;
           } ?>
       </tbody>
       </table>
          
         <?=$blocks[1]["DETAIL_TEXT"]; ?>
          </div>
          
          <!--       END TOP-5  -->
           
           
           <?=isset($blocks[3]["DETAIL_TEXT"]) ? '<h2>'.$blocks[2]["NAME"].'</h2>' . $blocks[2]["DETAIL_TEXT"] : ''; ?>
       <div class="block"><div class="carousel-wrapper">
                <div class="carousel-prev"></div>
                <div class="carousel-content">
                    <? 
                    $slider = array();
                    $i = 0;
                    
                    foreach ($companies as $value)
                    {
                        $slider[] = $value;
                        $i++;
                    }
                               
                    ?>
                    <ul>
                        <? $i = 0;
                           foreach ($slider as $value)
                        {
                         ?>
                        <li>
                            <a href="<?=('/' . $value["CODE"] . '/')?>" class="slider-marker no-top">
                              <div class="img-container<?=$active?>">
                                <?  $default_img = $config->getPicture("company_slide_picture");
                                    $img = tools::cut_picture($value["PREVIEW_PICTURE"], $value["NAME"], 
                                    $default_img["dimensions"], 
                                        $default_img["src"]);?>
                                <?=$img?>
                              </div>
                              <p>Рейтинг <span><?=$value["RATE"]?></span></p>
                            </a>
                        </li>
                        <? $i++;
                        }?>
                    </ul>
                </div>
                <div class="carousel-next"></div>
            </div> </div>   
    </div>
</section>


<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/epilog_before.php");
?>