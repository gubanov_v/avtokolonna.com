var gulp = require('gulp');
var autoprefixer = require('gulp-autoprefixer');
 
gulp.task('css', function () {
	return gulp.src('css/style.css')
		.pipe(autoprefixer({
			browsers: ['last 2 versions'],
			cascade: false
		}))
		.pipe(gulp.dest('css'));
});

gulp.task('media', function () {
	return gulp.src('css/media.css')
		.pipe(autoprefixer({
			browsers: ['last 2 versions'],
			cascade: false
		}))
		.pipe(gulp.dest('css'));
});