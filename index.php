<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_after.php");
echo '<script src="https://api-maps.yandex.ru/2.1/?lang=ru_RU" type="text/javascript"></script>';

$config = config::getInstance();

$this->addJsFile("index.js");

$dbCompainies = new iblock($config->getBlocksId("companies"));
$dbCompainies->setOrder(array("PROPERTY_RATE" => "DESC"));
$dbCompainies->setCount(15);
$companies = $dbCompainies->getList();

$ids = array();
foreach ($companies as $company)
    $ids[] = $company["ID"];
    
$review_count = array();
$dbReviews = new iblock($config->getBlocksId("reviews"));
$dbReviews->setFilter(array("PROPERTY_COMPANY" => $ids));
$reviews = $dbReviews->getList();
foreach ($reviews as $review)
{
    $key = $review["COMPANY"];
    if (!isset($review_count[$key])) $review_count[$key] = 0; 
    $review_count[$key]++;
}

$dbBlocks = new iblock($config->getBlocksId("blocks"));
$dbBlocks->setFilter(array("CODE" => array("rate_info", "update", "map", "order", "other", "h2")));
$dbBlocks->setUniq("CODE");
$blocks = $dbBlocks->getList();

?>

<section class="hello">
  <div class="fixblock">

       <h1 class="index"><?=$this->_h1?></h1>
       <?=$this->_raw_text?>
         </div>
       
       <table>
       <thead>
          <tr>
            <th><img src="<?=SITE_TEMPLATE_PATH?>/images/icon-cup.png" alt="Кубок" width="21" height="22"/></th>
            <th>Название компании</th>
            <th>Автопарк</th>
            <th>Отзывы</th>
            <th>Цены</th>
            <th>Время подачи</th>
            <th>Рейтинг <a href="#" class="a-info"><img src="<?=SITE_TEMPLATE_PATH?>/images/icon-info.png" alt="Инфо" width="15" height="15"/></a></th>
            <th><p class="text-lite">Обновлено <? if ($blocks["update"]["DETAIL_TEXT"]) echo strip_tags($blocks["update"]["DETAIL_TEXT"]); else echo date("d.m.Y");?></p></th>
            </tr>
        </thead>
        <tbody>
        <?
            $i = 1;
            foreach ($companies as $company) 
            {    
               if ($company["RATE_DYN"] == "Положительный")
                  $add = " icon-up";
               else
                if ($company["RATE_DYN"] == "Отрицательный")
                    $add = " icon-down";
                else
                    $add = "";
                    
               $add_sup = "";
               if ($company["RATE_CORRECT"] > 0)
               {
                    $add_sup = "+".(string) $company["RATE_CORRECT"];
                    $add_sup_class = "up";
               }
               
               if ($company["RATE_CORRECT"] < 0)
               {
                    $add_sup = (string) $company["RATE_CORRECT"];
                    $add_sup_class = "down";
               }
                    
               $add_sup = '<sup class="'.$add_sup_class.'">'.$add_sup.'</sup>';
               
               $company["LINK_NAME"] = ($company["LINK_NAME"]) ? $company["LINK_NAME"] : $company["NAME"];
        ?>
            <tr data-href="/<?=$company["CODE"]?>/">
                <td class="icon<?=$add?>"><?=$i?></td>
                <td><a href="<?=$company["LINK"]?>" class="link link-out" target="_blank"><?=$company["LINK_NAME"]?></a></td>
                <td>~<?=$company["AVTO"]?></td>
                <td><?=(($review_count[$company["ID"]]) ? $review_count[$company["ID"]] : 0)?></td>
                <td>от <?=$company["MIN_PRICE"]?> руб.</td>
                <td><?=$company["TIME"]?></td>
                <td><?=$company["RATE"]?><?=$add_sup?></td>
                <td><a href="/<?=$company["CODE"]?>/" class="btn btn-green btn-s btn-i-right">Подробнее</a></td>
            </tr>
        <? $i++;
           } ?>
       </tbody>
       </table>
           
       <div class="fixblock">
       <div class="more-info">
          
           <div class="item-article">
               <img src="<?=SITE_TEMPLATE_PATH?>/images/icon-info.png" alt="Инфо" width="15" height="15"/>
               <? if ($blocks["rate_info"]["DETAIL_TEXT"]): ?>
               <div class="info">
                   <?=$blocks["rate_info"]["DETAIL_TEXT"]?>
                </div>
               <? endif; ?>
            </div>
            <? $other_left = $other_right = $other_center = '<ul>';
            $dbCompainiesOther = new iblock($config->getBlocksId("companies"));
            $dbCompainiesOther->setFilter(array("!PROPERTY_OTHER" => false));
            foreach ($dbCompainiesOther->getList() as $value)
            {
                $t = '<li><a href="/'.$value["CODE"].'/">'.$value["NAME"].'</a></li>';
                switch ($value["OTHER"])
                {
                    case "Правая колонка":
                        $other_right .= $t;
                    break;
                    case "Левая колонка":
                        $other_left .= $t;
                    break;
                    case "Центральная колонка":
                        $other_center .= $t;
                    break;
                }
            }

            $other_left .= '</ul>';
            $other_center .= '</ul>';
            $other_right .= '</ul>';
            ?>
                <div class="other-companies-wrapper">
                    <div class="other-companies list">
                    <?=$other_left . $other_center . $other_right;?>
                    </div>
                </div>
                <a href="#" class="other-trigger link">Другие компании ▼</a>
            
       </div>
       </div>


</section>
<? if ($blocks["h2"]["DETAIL_TEXT"]):?>
<div class="second-bg">
       <div class="fixblock ">
           <div class="block">
             <div class="title">
             <h2><?=$blocks["h2"]["NAME"]?></h2>
                 
              <div class="line"></div>
              </div>
               <?=$blocks["h2"]["DETAIL_TEXT"]?>  
               

                <div class="item-card check">
                
                
                <a href="/bitrix/templates/avto/images/check-2.jpg" data-rel="lc" title="">
                  <img src="/bitrix/templates/avto/images/check-2-mini.jpg" alt="" width="289" height="180">
                </a>
                <a href="/bitrix/templates/avto/images/check-1.jpg" data-rel="lc" title="">
                  <img src="/bitrix/templates/avto/images/check-1-mini.jpg" alt="" width="289" height="180">
                </a>
                <a href="/bitrix/templates/avto/images/check-3.jpg" data-rel="lc" title="">
                  <img src="/bitrix/templates/avto/images/check-3-mini.jpg" alt="" width="289" height="180">
                </a>
                
<!--
                <img src="/bitrix/templates/avto/images/check-1.png" alt="">
                <img src="/bitrix/templates/avto/images/check-2.png" alt="">
                <img src="/bitrix/templates/avto/images/check-3.png" alt="">
-->
              </div>

                                                                     
           </div>
        </div>
     </div>
<? endif;?>
<div class="fixblock">

    <? if ($blocks["map"]):?>
     <div class="block">
       <div class="title">
          <h2><?=$blocks["map"]["NAME"]?></h2>
          <div class="line"></div>
        </div>
         <?=$blocks["map"]["DETAIL_TEXT"]?>
         <? 
            $dbCars = new iblock($config->getBlocksId("cars"));
            $dbCars->setUniq("ID");
            $cars = $dbCars->getList();
         
            $dbParkcars = new iblock($config->getBlocksId("parkcars"));
            foreach ($dbParkcars->getList() as $park)
            {
                $count = count($park["POINT"]);
                
                $time = floor(time() / (60 * 10));
                
                $point = explode(",", $park["POINT"][$time % $count]);
                                                    
                echo '<div class="park" data-x="'.$point[0].'" data-y="'.$point[1].'" data-car="'.htmlspecialchars($park["CAR_NAME"]).'"
                    data-company="'.htmlspecialchars($park["COMPANY_NAME"]).'" data-company-code="'.$park["COMPANY_CODE"].'"></div>';

            }
         ?>
         <div id="map" class="mymap" style="width: 100%; height: 580px"></div>
         
        
     </div>
     <? 
    endif;?>
    <div class="block"><div class="b-areas">
             <img src="<?=SITE_TEMPLATE_PATH?>/images/gerb-moscow.png" width="80px" height="auto" alt="">
            <div class="item-around">
                <div><a href="/evacuatory-sao/">Эвакуаторы САО</a>
                      <a href="/evacuatory-svao/">Эвакуаторы СВАО</a>
                      <a href="/evacuatory-vao/">Эвакуаторы ВАО</a>
                </div>
                      <div><a href="/evacuatory-uvao/">Эвакуаторы ЮВАО</a>
                      <a href="/evacuatory-uao/">Эвакуаторы ЮАО</a>
                 
                  
                  <a href="/evacuatory-uzao/">Эвакуаторы ЮЗАО</a></div>
                 
                      
                      
                  <div>
                      <a href="/evacuatory-zao/">Эвакуаторы ЗАО</a>
                      <a href="/evacuatory-szao/">Эвакуаторы СЗАО</a>
                      <a href="/evacuatory-cao/">Эвакуаторы ЦАО</a>
                  </div>
            </div>
        
          </div> </div>
       
    <? if ($blocks["order"]):?> 
      <div class="block">
       <div class="title">
          <h2><?=$blocks["order"]["NAME"]?></h2>
          <div class="line"></div>
        </div>
        <?=$blocks["order"]["DETAIL_TEXT"]?>
     </div>
    <? endif; ?>
</div>

<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/epilog_before.php");
?>

