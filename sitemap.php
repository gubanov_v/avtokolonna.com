<? 
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_after.php");

$config = config::getInstance(); ?>

<section class="hello">
      <div class="fixblock">
          <div class="crumbs">
            <a href="/"><img src="<?=SITE_TEMPLATE_PATH?>/images/icon-home.png" alt="Главная" title="Главная" width="17" height="15"/></a>
            <a href="/<?=config::getInstance()->getArg()?>/"><?=$this->_name?></a>
          </div>

          <h1><?=$this->_h1?></h1> 
          <article>
            <div class="list">
            <ul>
                <li><a href="/">Главная</a></li>
                <?
                    global $menu;
                    foreach ($menu->getArray(true) as $key => $value)
                    {   
                        echo '<li><a href="/'.$value.'/">'.$key.'</a></li>'; 
                    }
                    
                    global $accord;
                    $keys = array();
                    $paths = array();
                    
                    $dbPath = new level;
                    $dbPath->setIblock(array_keys($accord));
                    $dbPath->setValue(array("NAME", "CODE", "IBLOCK_ID"));
                    foreach ($dbPath->getList() as $value)
                    {
                        echo '<li><a href="/'.$value["CODE"].'/">'.$value["NAME"].'</a></li>';
                    }
                ?>
            </ul>
            </div>
            <div class="clear"></div>
          </article>      
    </div>
</section>

<? require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/epilog_before.php"); ?>