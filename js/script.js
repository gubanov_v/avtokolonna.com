$(document).ready(function () {
    
    
$('.diplomas a').click(function(){
        $t = $(this);

        ww = $t.data("width");
        wh = $t.data("height");
                        
        str = '<a href="#modal-diplomas" class="overlay" id="modal-diplomas-target"></a>';
        str += '<div id="modal-diplomas" class="modal modal-diplomas">';
        str += '<p class="h1">' + $t.data("name") + '</p>';
        str += '<div class="diplomas-wrapper">';
        str += '<img src="' + $t.data("poster") + '">';
        str += '</div>';
        str += '<a class="close close-black" title="Закрыть" href="#close"></a></div>';
        
        $("body").append(str); 
        
        resize_diplomas();
        $(window).bind("resize", resize_diplomas); 
    });
    
    $("body").on("click", "#modal-diplomas .close", function(){
         $("#modal-diplomas-target, #modal-diplomas").remove();
          $(window).unbind("resize");
    });

});